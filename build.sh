#!/bin/bash

src_dir="html_root"
wdir=$(dirname $0)
docker_file=${wdir}/Dockerfile.nginx-auto

if [ -z "${1}" ]; then
	echo "Please give a container name like: blue_potatoe:2.1"
	exit
fi

echo "Using this roo web directory: $src_dir"
sleep 2

echo "HTAccess users in place are:"
cat ${wdir}/htaccess | sed -e "s/:.*//" -e "s/^/ -> /"
sleep 2

echo " ------------------------- "
# Build and remove intermediate images
docker build --force-rm -f $docker_file -t $1 $wdir

echo " ------------------------- "
core_image_name=$(echo -n $1 | sed -e "s/:.*//")
docker image ls | grep $core_image_name


echo " ------------------------- "
echo ""
echo " # To deploy ad this image ${core_image_name} image in your next docker-compose file and hit:"
echo ""
echo " >>> celestical deploy "
echo ""
echo " # Or do whatever more complicated stuffs other cloud providers tell you to do."
echo ""
echo " ------------------------- done."
