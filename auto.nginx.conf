user nginx nginx;
worker_processes 1;
    
error_log /var/log/nginx/error_log info;
    
events {
    worker_connections 512;
    use epoll;
} 

http {
    include /etc/nginx/mime.types;
    default_type application/octet-stream;
    real_ip_header    X-Forwarded-Proto;

    log_format main
        '$remote_addr,$http_x_forwarded_for,$time_iso8601,$request_time,'
        '"$request",$status,$bytes_sent,'
        '"$http_referer","$http_user_agent",'
        '"$gzip_ratio"';

    tcp_nopush on;
    tcp_nodelay on;
    port_in_redirect off;

    server {
        listen      80;
        listen [::]:80;

        # Enable gzip but do not remove ETag headers
        gzip on;
        gzip_vary on;
        gzip_comp_level 4;
        gzip_min_length 256;
        gzip_proxied expired no-cache no-store private no_last_modified no_etag auth;
        gzip_types application/atom+xml application/javascript application/json application/ld+json application/manifest+json application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;

        location / {
            root /usr/share/nginx/html/public; 
            index index.html;
            expires 1M;
        }

        location /logs {
            root /usr/share/nginx/html/logs; 
            index index.html;
            autoindex on;
            autoindex_exact_size on;
            autoindex_format html;
            autoindex_localtime on;
            expires 1M;

            auth_basic "Admin Area - Reserved for Authorized Personel only";
            auth_basic_user_file /etc/.htpasswd;
        }

        # Example of IP filtering
        #  deny  192.168.1.2;
        #  allow 192.168.1.1/24;
        #  allow 127.0.0.1;
        #  deny  all;
        #  
        #  location ~ \.(?:css|js|png|ttf|woff|woff2|eot|mp4|map|less|svg|gif)$ {
        #      expires 1M;         # Cache-Control policy borrowed from `.htaccess`
        #  }

        access_log  /var/log/nginx/access_log  main;
        error_log   /var/log/nginx/error_log   warn;

        location ~ /\.ht {
            deny  all;
        }

    }
}
