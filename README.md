# nginx-autoindex-htaccess

This container helps you quickly serve the content of one directory via a webserver
and allow you to set user/password access using the http basic method.


## Getting started

Prior to building your image you need to build an htaccess file

With a random image name:

```bash

bash build.sh image_name:1.2

```


## Going further and important notes

 - Do not add an index.html to the main root unless you want to remove the
   autoindex capability from that folder. The 'index.html' would overwrite the
   autoindex behaviour.
 - You can adjust the nginx configuration for anything else you need to give.
